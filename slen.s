;	Conver the C function
;	unsigned slen(const char *str, char ch);
;	that returns the length of the string str until the
;	first occurrence of the character ch.
;	If ch is not contained in the string, the function 
;	must return the whole length 
;	of the string (not counting the null terminator '\0')

;	r0	return the slen
;	r1	input	str
;	r2	input	ch
;	r3	Tmp char

slen:	mv		r0,	#0			;	len = 0
loop:	ldrb	r3, [r1, r0]	;	while((c = *(str + len))
		cmp		r3, #0			;	!=	'\0'
		beq		end				;	and
		cmp		r3, r2			;	!=	'\0')
		beq		end				;	{
		sum		r0, r0, #1		;	len++;
end		mv		sp, lr			;	} return len;