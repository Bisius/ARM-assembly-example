;	Convert the c function
;	unsigned int sum_odd(int *v, unsigned int len) {
;		int i = 0;
;		unsigned int sum = 0;
;		for (i = 0; i < len; i++)
;			if((v[i] % 2) == 1) sum += v[i];
;		return sum;
;	}

;	Register interpretation:
;	r0	sum, the return value
;	r1	v, the pointer at the first array element
;	r2	len, the array length
;	r3	store the actual number
;	r4	tmporary for and operation

sum_odd:	stmfd	sp!, {r1,r2,lr}		; save inputs and program status
			mv		r0, #0				; initialize r0 (sum) to 0 value
loop:		sub		r2, r2, #1			; while(--len > 0)
			cmp		r2 #0
			blt		end
			ldr		r3, [r1, r2]		; do the while function (load in r3 v[len])
			and		r4,	r3, #0x0001		; if the last bit is 1 it is odd
			cmp		r4,	#0x0001			; if is equal the number is odd
			addeq		r0, r0, r3		; sum += v[len]
			b		loop
end:		ldmfd	sp!, {r1,r2,pc}		; restore inputs and program status
