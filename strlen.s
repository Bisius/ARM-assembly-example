;   Convert the C standard function strlen
;   From a char pointer return the string length
;   int strlen(char* s);
;	r0	Output, the string length
;	r1	Input, the string pointer
;	r2	Tmp char

strlen:	mov		r0,	#0			;	len = 0
loop:	ldrb	r2, [r0, r1]	;	while(c = *(s + len)
		cmp		r2, #0			;	
		beq		end				;	!=	'\0') {	// '\0' is 0x00
		sum		r0, r0, #1		;	len++;
		b		loop			;	}
end:	mv		pc, lr			;	return